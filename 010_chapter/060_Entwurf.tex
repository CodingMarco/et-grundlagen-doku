\section{Entwurf der Thermometerschaltung}
\label{sec:Entwurf}

\subsection{Simulation der Wheatstone-Brücke}
\label{subsec:wheatstone_simulation}
\begin{figure}[ht]
    \centering
    \scalebox{0.7}{\includegraphics[width=\textwidth]{020_images/060_Entwurf/WheatstoneBridge.png}}
    \caption{Schaltungssimulation der Wheatstone-Brücke mit LTSpice}
    \label{fig:WheatstoneSimulation}
\end{figure}
Im ersten Schritt soll die Wheatstone-Brücke der Temperaturschaltung simuliert werden.
In Abbildung \ref{fig:WheatstoneSimulation} sieht man den in LTSpice gezeichneten Schaltplan, aus dem ebenfalls die
Widerstandswerte $R_1$, $R_2$ und $R_3$ entnommen werden können. $R_{pt}$ stellt den PT1000 dar.
Dieser hat nach \eqref{eq:RT_PT1000} bei einer Temperatur von 22 \textdegree{}C einen Widerstand von ca. 1085\textOmega.
$v1$ und $v2$ sind Messpunkte der Spannung im Bezug zur Masse.
Die Spannung, die in Abbildung \ref{fig:WheatstoneSimulation} neben der Schaltung zu sehen ist, errechnet sich durch $v2 - v1$.
Der Gesamtwiderstand der Schaltung kann nun durch das Ohm'sche Gesetz $R_{ges} = \frac{U_{ges}}{I_{ges}}$ berechnet werden.
Da die Gesamtspannung mit $U_{ges} = 5V$ bereits gegeben ist, muss nur noch der Gesamtstrom $I_{ges}$, den die Spannungsquelle liefert, in LTSpice gemessen werden.
So misst man einen Strom von $|I_{ges}| = 2mA$.
Damit ergibt sich ein Gesamtwiderstand von
\begin{center}
    \boxed{R_{ges} = \frac{U_{ges}}{I_{ges}} = \frac{5V}{2mA} = 2,5k\Omega}
\end{center}
\pagebreak
Als nächstes sind die Ströme durch alle Widerstände zu ermitteln. Diese können in LTSpice gemessen werden. Es ergibt sich:
\begin{itemize}
    \item $I_1 = I_2 = 0,93mA$
    \item $I_3 = I_{pt} = 1,1mA$
\end{itemize}
\par Aus den Strom- und Widerstandswerten lassen sich nun auch die Spannungsabfälle über den Widerständen berechnen,
in dem man sie in $U = R \cdot I$ einsetzt:
\begin{itemize}
    \item $U_1 = R_1 \cdot I_1 = 3,6k\Omega \cdot 0,93mA = 3,35V$
    \item $U_2 = R_2 \cdot I_2 = 1,8k\Omega \cdot 0,93mA = 1,67V$
    \item $U_3 = R_3 \cdot I_3 = 3,6k\Omega \cdot 1,1mA = 4V$
    \item $U_{pt} = R_{pt} \cdot I_{pt} = 1085\Omega \cdot 1,1mA = 1,2V$
\end{itemize}
\par Schließlich kann auch die Leistungsaufnahme der Widerstände durch $P = U \cdot I$ berechnet werden:
\begin{itemize}
    \item $P_1 = U_1 \cdot I_1 = 3,35V \cdot 0,93mA \approx 3,1mW$
    \item $P_2 = U_2 \cdot I_2 = 1,67V \cdot 0,93mA \approx 1,6mW$
    \item $P_3 = U_3 \cdot I_3 = 4V \cdot 1,1mA = 5,5mW$
    \item $P_{pt} = U_{pt} \cdot I_{pt} = 1,2V \cdot 1,1mA = 1,32mW$
\end{itemize}
\par Im Bezug auf das Ziel, einen linearen Zusammenhang zwischen Temperatur und Ausgangsspannung herzustellen
ist es außerdem interessant, genau diesen Zusammenhang darzustellen. Dies ist mit LTSpice möglich.
Dafür wird in LTSpice eine Variable $T$ eingeführt, die die Temperatur darstellt und im vorgegebenen Bereich von -15 bis 55 °C
in 1 °C Schritten erhöht wird. Für jeden Schritt wird nun der Widerstand $R_{pt}$ mit der Formel \eqref{eq:RT_PT1000} berechnet
und die Spannungsdifferenz $v_2-v_1$ gemessen. Wird die Temperatur auf der x-Achse und die Spannungsdifferenz auf der y-Achse
aufgetragen, erhält man das Diagramm, welches in Abbildung \ref{fig:U-T_WheatstonePT1000} dargestellt ist.
\begin{figure}[ht]
    \centering
    \scalebox{1}{\includegraphics[width=\textwidth]{020_images/060_Entwurf/WheatstonePT1000UT.png}}
    \caption{U(T) Diagramm eines PT1000 in einer Wheatstone-Brücke}
    \label{fig:U-T_WheatstonePT1000}
\end{figure}
Aus dem Diagramm kann man zwei wichtige Eigenschaften der bisherigen Schaltung ableiten:
Zum einen besteht zwischen Temperatur und Ausgangsspannung bereits ein linearer Zusammenhang.
Zum anderen liegt zwischen minimaler und maximaler Spannung im vorgegebenen Temperaturintervall nur $222mV$.
Dies entspricht nur ca. $0,0032\frac{V}{^\circ{}C}$. So können sehr schnell Messfehler auftreten und die errechnete Temperatur
kann stark von der tatsächlichen abweichen.
Die Ausgangsspannung der Wheatstone-Brücke muss folglich verstärkt werden.
Dies kann man durch Operationsverstärker erreichen. Operationsverstärker haben den Vorteil, dass diese einen sehr hohen
Eingangswiderstand haben und man die Wheatstone-Brücke somit als zwei \textit{unbelastete} Spannungsteiler betrachten kann.
Der unbekannte Widerstand durch \eqref{eq:wheatstone_unbelastet_gesamt} berechnet werden.

\subsection{Mathematische Beziehungen der Wheatstone-Brücke mit konkreten Widerstandswerten}
\label{subsec:wheatstone_konkret_mathematisch}
Im folgenden werden Formeln aufgestellt, die für die Berechnungen der Gesamtschaltung wichtig sind.
Dabei sind die Widerstandswerte $R_1$, $R_2$ und $R_3$ entsprechend Abbildung \ref{fig:WheatstoneSimulation}
festgelegt. $R_{pt}$ wird als Variable betrachtet.
Für das Potenzial $v_2$ des rechten Spannungsteilers im Bezug zur Masse gilt:
\begin{equation}
    \label{eq:wheatstone_spannungsteiler_v2}
    \boxed{v_2 = V1 \cdot \frac{R_2}{R_2 + R_1} = 5V \cdot \frac{1,8k\Omega}{1,8k\Omega + 3,6k\Omega} = 1,\overline{6}V}
\end{equation}
Für das Potenzial $v_1$ des linken Spannungsteilers gilt entsprechend:
\begin{equation}
    \label{eq:wheatstone_spannungsteiler_v1}
    \boxed{v_1 = V1 \cdot \frac{R_{pt}}{R_{pt} + R_3} = 5V \cdot \frac{R_{pt}}{R_{pt} + 3,6k\Omega}}
\end{equation}

\subsection{Verstärkung der Spannung der Wheatstone-Brücke}
\label{subsec:amplification}
Für die Verstärkung kommen verschiedene Operationsverstärkerschaltungen in Frage.
Am einfachsten scheint ein Differenzverstärker zu sein, da dieser nur einen Operationsverstärker benötigt.
Allerdings werden bei einem solchen die Signalquellen belastet und so könnten die Spannungsteiler der Wheatstone-Brücke
nicht mehr als unbelastet betrachtet werden. Mit zwei vorgeschalteten Impedanzwandlern würde man insgesamt drei
Operationsverstärker benötigen, dies lässt sich vermeiden.
Um das Problem zu vereinfachen kann man vorerst nur die Spannung $v_1$ im Bezug zur Masse betrachten, 
da sich ausschließlich diese mit der Temperatur ändert, während $v_2$ gleich bleibt.
Die Spannung $v_1$ kann mit einem nicht-invertierenden Verstärker verstärkt werden. Dieser erfüllt auch die Anforderung,
dass er einen hohen Eingangswiderstand hat, da das Eingangssignal direkt am Operationsverstärker anliegt.
Bis zu diesem Punkt sieht die Schaltung so aus, wie in Abbildung \ref{fig:WheatstoneBridge_NoninInvAmp} dargestellt.

\begin{figure}[ht]
    \centering
    \scalebox{0.9}{\includegraphics[width=\textwidth]{020_images/060_Entwurf/WheatstoneBridge_NonInvAmp.png}}
    \caption{Wheatstone-Brücke mit einem nicht-invertierenden Verstärker}
    \label{fig:WheatstoneBridge_NoninInvAmp}
\end{figure}

Die Ausgangsspannung $U_1$ kann mit der Formel für den\linebreak nicht-invertierenden Verstärker berechnet werden:
\begin{equation}
    \label{eq:Thermometer_NonInvAmp}
    \boxed{U_1 = v_1 \cdot \frac{R_2 + R_3}{R_2} = v_1 \cdot \frac{3,9k\Omega + 1k\Omega}{3,9k\Omega} = \frac{49}{39}v_1}
\end{equation}

Da der Operationsverstärker einen geringen Ausgangswiderstand hat, ist es nun kein Problem, dessen Ausgangssignal $U_1$
in einen weiteren abgewandelten invertierenden Verstärker zu leiten. Abgewandelt deshalb, da am nicht-invertierenden Eingang
die in \eqref{eq:wheatstone_spannungsteiler_v2} berechnete, konstante Spannung $v_2 = 1,\overline{6}V$
aus dem rechten Spannungsteiler der Wheatstone-Brücke anliegt.

\pagebreak
Die vollständige Thermometerschaltung sieht also so aus, wie in Abbildung \ref{fig:Thermometer_noCapacitors} dargestellt.

\begin{figure}[ht]
    \centering
    \scalebox{1}{\includegraphics[width=\textwidth]{020_images/060_Entwurf/Thermometer_noCapacitors.png}}
    \caption{Die resultierende Thermometerschaltung}
    \label{fig:Thermometer_noCapacitors}
\end{figure}

Um die Übertragungsfunktion dieses abgewandelten invertierenden Verstärkers zu entwickeln, stellt man zuerst Gleichungen für die Ströme
$I_1$ durch $R_1$ und $I_4$ durch $R_4$ auf. Dafür kann man davon ausgehen, dass der Operationsverstärker dafür sorgt,
dass die Potenziale an seinen Eingängen gleich sind. Das Potenzial am invertierenden Eingang ist also auch $1,\overline{6}V$.

\begin{center}
\begin{subequations}
\begin{tabularx}{\textwidth}{Xp{1cm}X}
        \begin{equation}
            \label{eq:Thermometer_I1}
            I_1 = \frac{1,\overline{6}V - U_1}{R_1}
        \end{equation}
        & &
        \begin{equation}
            \label{eq:Thermometer_I2}
            I_4 = \frac{U_{out} - 1,\overline{6}V}{R_4}
        \end{equation}
\end{tabularx}
\end{subequations}
\end{center}

Aufgrund des sehr hohen Eingangswiderstands des Operationsverstärkers und der Knotenregel kann man ebenfalls davon ausgehen,
dass $I_1 = I_4$ ist. Setzt man \eqref{eq:Thermometer_I1} und \eqref{eq:Thermometer_I2} in diese Gleichung ein und löst nach
$U_{out}$ auf ergibt sich:

\begin{equation}
    \label{eq:Thermometer_right_amp}
    \boxed{U_{out} = \left(1,\overline{6}V - U_1\right) \cdot \frac{R_4}{R_1} + 1,\overline{6}V}
\end{equation}

Wie man aus der Gleichung \eqref{eq:Thermometer_right_amp} ablesen kann, würde die Ausgangsspannung $U_{out}$ negativ werden,
wenn $U_1 > 1,\overline{6}V$ wird. Da der Operationsverstärker jedoch nur eine positive Versorgungsspannung hat, muss
$U_1 \leq 1,\overline{6}V$ bleiben, damit die auf die Versorgungsspannung limitierte Ausgangsspannung nicht verfälscht wird.
Setzt man die Grenzen des Temperaturintervalls -15\textdegree{}C und 55\textdegree{}C in \eqref{eq:RT_PT1000} ein, die
Ergebnisse in \eqref{eq:wheatstone_spannungsteiler_v1}, und diese wiederum in \eqref{eq:Thermometer_right_amp},
erhält man für $U_1$ die Werte $1,3V$ und $1,58V$. Da diese kleiner als $1,\overline{6}V$ sind, können alle Temperaturen im
festgelegten Temperaturintervall korrekt bestimmt werden.

\subsection{Ausgleichung von Fluktuationen der Netzspannung}
Wird die Thermometerschaltung nicht mit einem hochwertigen Netzteil betrieben, sondern beispielsweise
mit der 5V-USB-Spannung, können in der Versorgungsspannung Fluktuationen auftreten. Da nach
\eqref{eq:wheatstone_spannungsteiler_v1} die Ausgangsspannung des Spannungsteilers mit dem PT1000 proportional zur
Versorgungsspannung ist, würden solche Fluktuationen die Genauigkeit der Temperaturmessungen beeinträchtigen.
Gleiches gilt auch für die Operationsverstärker, da deren maximale Ausgangsspannung auf die Versorgungsspannung limitiert ist.
Um diese Fluktuationen auszugleichen, werden parallel zur Versorgungsspannung der Wheatstone-Brücke sowie der beiden Operationsverstärker
100nF Kondensatoren geschaltet. Diese sollten physisch so nah wie möglich an der Wheatstone-Brücke bzw. den Operationsverstärkern
angebracht werden um auch Fluktuationen, die in den (realen) Leitungen auftreten, auszugleichen.
Wie die Kondensatoren letztendlich auf der Platine angebracht sind, ist in Abbildung \ref{fig:Layout_Lochrasterplatine} zu sehen.

\subsection{Aufstellung der Übertragungsfunktion für die gesamte Thermometerschaltung}
\label{subsec:Thermometer_TransferFunction}
Nun, da für alle Schaltungsteile mathematische Beziehungen aufgestellt wurden, können diese zu einer Übertragungsfunktion
für die gesamte Schaltung in Abbildung \ref{fig:Thermometer_noCapacitors} zusammengeführt werden.
Dafür wird für $U_1$ \eqref{eq:Thermometer_NonInvAmp} in \eqref{eq:Thermometer_right_amp} eingesetzt. Man erhält:

\begin{equation}
    \label{eq:Thermometer_with_U1}
    U_{out} = \left(1,\overline{6}V - \frac{49}{39}v_1\right) \cdot \frac{R_4}{R_1} + 1,\overline{6}V
\end{equation}

Anschließend wird für $v_1$ in \eqref{eq:Thermometer_with_U1} noch \eqref{eq:wheatstone_spannungsteiler_v1} eingesetzt:

\begin{equation}
    \label{eq:Thermometer_with_v1}
    U_{out} = \left(1,\overline{6}V - \frac{49}{39} \cdot \left(5V \cdot \frac{R_{pt}}{R_{pt} + 3,6k\Omega}\right)\right) \cdot \frac{R_4}{R_1} + 1,\overline{6}V
\end{equation}

Im letzten Schritt wird für $R_{pt}$ noch \eqref{eq:RT_PT1000} eingesetzt, um einen direkten Zusammenhang zwischen
der Temperatur $T$ und der Ausgangsspannung $U_{out}$ herzustellen:

\begin{equation}
    \label{eq:Thermometer_ges}
    \boxed{U_{out} = \left(1,\overline{6}V - \frac{49}{39} \cdot \left(5V \cdot \frac{1k\Omega + 3,851T}{1k\Omega + 3,851T + 3,6k\Omega}\right)\right) \cdot \frac{R_4}{R_1} + 1,\overline{6}V}
\end{equation}

Setzt man nun in \eqref{eq:Thermometer_ges} noch Werte für $R_4$ und $R_1$ entsprechend Abbildung
\ref{fig:Thermometer_noCapacitors} ein und vereinfacht die Gleichung, erhält man:

\begin{equation}
    \label{eq:Thermometer_ges_vereinfacht}
    \boxed{U_{out} = -24,5 \cdot \left(1 - \frac{3600\Omega}{3,851T + 4600\Omega}\right) + \frac{49}{6}V}
\end{equation}

\subsection{Simulation der gesamten Thermometerschaltung}
\label{subsec:Thermometer_simulation_ges}
Im Folgenden wird mithilfe von LTSpice die gesamte Thermometerschaltung entsprechend Abbildung \ref{fig:Thermometer_noCapacitors}
simuliert. Es werden die Spannungen $v_1$, $U_1$ und $U_{out}$, sowie die Ströme $I_{R_1}$ durch $R_1$ und $I_{ges}$,
der von der Spannungsquelle $V1$ geliefert wird, gemessen. Die Simulationsergebnisse sind in Tabelle \ref{tab:thermometer_simulation}
aufgelistet.

\begin{table}[ht]
\centering
\begin{tabular}{l|lllll}
           & -15 \textdegree C & 0 \textdegree C & 20 \textdegree C & 21 \textdegree C & 55 \textdegree C \\  \hline
    $v_1$       & 1,037V  & 1,087V & 1,15V  & 1,155V  & 1,259V  \rule{0pt}{2.6ex} \\[5pt]
    $U_1$       & 1,303V  & 1,366V & 1,447V & 1,451V  & 1,582V  \\[5pt]
    $U_{out}$   & 3,084V  & 2,84V  & 2,525V & 2,51V   & 1,996V  \\[5pt]
    $I_{R_1}$   & 363,5\textmu A & 301\textmu A  & 220\textmu A  & 216,1\textmu A & 84,59\textmu A \\[5pt]
    $|I_{ges}|$ & 3,7mA   & 3,68mA & 3,66mA & 3,66mA  & 3,647mA
\end{tabular}
\caption{Simulationsergebnisse der Thermometerschaltung}
\label{tab:thermometer_simulation}
\end{table}

Mit LTSpice kann ebenfalls ein Diagramm wie das in Abbildung \ref{fig:U-T_WheatstonePT1000} für die Ausgangsspannung
$U_{out}$ der Gesamtschaltung erstellt werden. Das Ergebnis ist in Abbildung \ref{fig:U-T_Gesamtschaltung} zu sehen.
\begin{figure}[ht]
    \centering
    \scalebox{1}{\includegraphics[width=\textwidth]{020_images/060_Entwurf/SimulationCurveGes.png}}
    \caption{U(T)-Diagramm der Gesamtschaltung}
    \label{fig:U-T_Gesamtschaltung}
\end{figure}

Das in Kapitel \ref{sec:Einleitung} definierte Ziel, einen möglichst linearen Zusammenhang
zwischen Temperatur und Ausgangsspannung herzustellen, wurde erreicht.